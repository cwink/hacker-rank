#include <cw.hpp>
#include <hr_cpp.hpp>

#include <array>
#include <iostream>
#include <limits>
#include <optional>
#include <type_traits>

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iterator>
#include <map>
#include <memory>
#include <unordered_map>

auto main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) -> int {
  hr::cpp::attribute_parser::run();

  return cw::exit_success;
}

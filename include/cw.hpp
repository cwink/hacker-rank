#ifndef CW_HPP
#define CW_HPP

#include <cstdlib>

namespace cw {
inline constexpr auto exit_failure{EXIT_FAILURE};

inline constexpr auto exit_success{EXIT_SUCCESS};
} // namespace cw

#endif // CW_HPP

find_program(CW_CLANG_TIDY NAMES clang-tidy)

if(CW_CLANG_TIDY)
  file(GLOB_RECURSE CW_ALL_SOURCES *.cpp *.hpp *.tpp)

  add_custom_target(
    tidy
    COMMAND
      ${CW_CLANG_TIDY}
      --checks="*,-fuchsia-trailing-return,-readability-named-parameter,-fuchsia-overloaded-operator,-hicpp-exception-baseclass,-llvm-qualified-auto,-readability-qualified-auto,-fuchsia-default-arguments-calls,-cppcoreguidelines-pro-bounds-constant-array-index,-hicpp-signed-bitwise,-bugprone-signed-char-misuse,-readability-magic-numbers,-cppcoreguidelines-avoid-magic-numbers,-clang-diagnostic-unused-macros,-cppcoreguidelines-owning-memory,-cppcoreguidelines-pro-type-reinterpret-cast"
      -header-filter=.* ../include/*.hpp ../include/*.tpp ../source/*.cpp
      ../source/*.ipp -extra-arg=-std=c++${CW_STANDARD})
endif()

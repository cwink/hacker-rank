#include <hr_cpp.hpp>

#include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

namespace hr::cpp::hello_world {
auto run() noexcept -> void { std::cout << "Hello, World!\n"; }
} // namespace hr::cpp::hello_world

namespace hr::cpp::basic_data_types {
auto run() noexcept -> void {
  auto a{0};

  auto b{0L};

  auto c{'\0'};

  auto d{0.0F};

  auto e{0.0};

  scanf("%d %ld %c %f %lf", &a, &b, &c, &d, &e); // NOLINT

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdouble-promotion"
  printf("%d\n%ld\n%c\n%f\n%lf\n", a, b, c, d, e); // NOLINT
#pragma GCC diagnostic pop
}
} // namespace hr::cpp::basic_data_types

namespace hr::cpp::input_and_output {
auto run() noexcept -> void {
  auto a{0};

  auto b{0};

  auto c{0};

  std::cin >> a >> b >> c;

  std::cout << (a + b + c) << '\n';
}
} // namespace hr::cpp::input_and_output

namespace hr::cpp::conditional_if_else {
auto run() noexcept -> void {
  auto mapper = std::unordered_map<int, std::string>(
      {{1, "one"}, {2, "two"}, {3, "three"}, {4, "four"}, {5, "five"}, {6, "six"}, {7, "seven"}, {8, "eight"}, {9, "nine"}});

  auto n{0};

  std::cin >> n;

  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

  if (1 <= n && n <= 9) {
    std::cout << mapper[n] << '\n';
  } else {
    std::cout << "Greater than 9\n";
  }
}
} // namespace hr::cpp::conditional_if_else

namespace hr::cpp::for_loop {
auto run() noexcept -> void {
  auto mapper = std::unordered_map<int, std::string>(
      {{1, "one"}, {2, "two"}, {3, "three"}, {4, "four"}, {5, "five"}, {6, "six"}, {7, "seven"}, {8, "eight"}, {9, "nine"}});

  auto a{0};

  auto b{0};

  std::cin >> a >> b;

  std::cin.ignore(std::numeric_limits<int>::max(), '\n');

  for (auto n{a}; n <= b; ++n) {
    if (1 <= n && n <= 9) {
      std::cout << mapper[n] << '\n';
    } else if (n > 9) {
      if (n % 2 == 0) {
        std::cout << "even\n";
      } else {
        std::cout << "odd\n";
      }
    }
  }
}
} // namespace hr::cpp::for_loop

namespace hr::cpp::functions {
[[nodiscard]] static auto max_of_four(const int a, const int b, const int c, const int d) noexcept -> int {
  auto arr = std::array<int, 4>{a, b, c, d};

  std::sort(std::begin(arr), std::end(arr), std::greater<>());

  return arr[0];
}

auto run() noexcept -> void {
  auto a{0};

  auto b{0};

  auto c{0};

  auto d{0};

  std::cin >> a >> b >> c >> d;

  std::cin.ignore(std::numeric_limits<int>::max(), '\n');

  auto ans{max_of_four(a, b, c, d)};

  std::cout << ans << '\n';
}
} // namespace hr::cpp::functions

namespace hr::cpp::pointer {
[[maybe_unused]] static auto update_unsafe(int *a, int *b) noexcept -> void {
  const auto t{a};

  *a += *b;

  const auto r{*t - *b};

  *b = r < 0 ? (r * -1) : r;
}

static auto update(int &a, int &b) noexcept -> void { // NOLINT
  const auto t{a};

  a += b;

  const auto r{t - b};

  b = r < 0 ? (r * -1) : r;
}

auto run() noexcept -> void {
  auto a{0};

  auto b{0};

  std::cin >> a >> b;

  std::cin.ignore(std::numeric_limits<int>::max(), '\n');

  update(a, b);

  // update_unsafe(&a, &b);

  std::cout << a << '\n' << b << '\n';
}
} // namespace hr::cpp::pointer

namespace hr::cpp::arrays_introduction {
auto run() noexcept -> void {
  auto n{0};

  std::cin >> n;

  std::cin.ignore(std::numeric_limits<int>::max(), '\n');

  auto vec = std::vector<int>();

  vec.reserve(static_cast<std::vector<int>::size_type>(n));

  for (auto i{0}; i < n; ++i) {
    auto a{0};

    std::cin >> a;

    vec.emplace_back(a);
  }

  std::cin.ignore(std::numeric_limits<int>::max(), ' ');

  std::reverse(std::begin(vec), std::end(vec));

  std::copy(std::cbegin(vec), std::cend(vec), std::ostream_iterator<int>(std::cout, " "));
}
} // namespace hr::cpp::arrays_introduction

namespace hr::cpp::variable_sized_arrays {
auto run() noexcept -> void {
  auto n{0};

  auto q{0};

  std::cin >> n >> q;

  std::cin.ignore(std::numeric_limits<int>::max(), '\n');

  auto a = std::vector<std::vector<int>>();

  a.reserve(static_cast<std::vector<std::vector<int>>::size_type>(n));

  for (auto i{0}; i < n; ++i) {
    auto k{0};

    std::cin >> k;

    std::cin.ignore(std::numeric_limits<int>::max(), ' ');

    auto vec = std::vector<int>();

    vec.reserve(static_cast<std::vector<int>::size_type>(k));

    for (auto j{0}; j < k; ++j) {
      auto m{0};

      std::cin >> m;

      if (j + 1 != k) {
        std::cin.ignore(std::numeric_limits<int>::max(), ' ');
      } else {
        std::cin.ignore(std::numeric_limits<int>::max(), '\n');
      }

      vec.emplace_back(m);
    }

    a.emplace_back(std::move(vec));
  }

  auto b = std::vector<std::pair<int, int>>();

  for (auto i{0}; i < q; ++i) {
    auto k{0};

    auto l{0};

    std::cin >> k >> l;

    std::cin.ignore(std::numeric_limits<int>::max(), '\n');

    b.emplace_back(std::make_pair(k, l));
  }

  for (const auto &item : b) {
    std::cout << a[static_cast<std::vector<std::vector<int>>::size_type>(item.first)][static_cast<std::vector<int>::size_type>(item.second)] << '\n';
  }
}
} // namespace hr::cpp::variable_sized_arrays

namespace hr::cpp::variadics {
template <auto N, bool... Digits> struct Check_values {
  static auto check(const int x, const int y) -> void {
    Check_values<N - 1, false, Digits...>::check(x, y);

    Check_values<N - 1, true, Digits...>::check(x, y);
  }
};

template <bool... Digits> struct Check_values<0, Digits...> {
  static auto check(const int x, const int y) -> void {
    auto z{reversed_binary_value<Digits...>()};

    std::cout << (z + 64 * static_cast<std::uint64_t>(y) == static_cast<std::uint64_t>(x));
  }
};

auto run() noexcept -> void {
  auto t{0};

  std::cin >> t;

  for (auto i{0}; i != t; ++i) {
    auto x{0};

    auto y{0};

    std::cin >> x >> y;

    Check_values<6>::check(x, y);

    std::cout << '\n';
  }
}
} // namespace hr::cpp::variadics

namespace hr::cpp::bit_array {
auto run() noexcept -> void {
  auto n{0};

  auto s{0};

  auto p{0};

  auto q{0};

  std::cin >> n >> s >> p >> q;

  std::cin.ignore(std::numeric_limits<int>::max(), ' ');

  auto previous{s % std::numeric_limits<std::int32_t>::max()};

  auto i{0};

  for (i = 1; i <= n - 1; ++i) {
    auto result{(previous * p + q) % std::numeric_limits<std::int32_t>::max()};

    if (result == previous) {
      i = i - 1;

      break;
    }

    previous = result;
  }

  std::cout << i << '\n';
}
} // namespace hr::cpp::bit_array

namespace hr::cpp::string_stream {
[[nodiscard]] static auto parse_ints(const std::string &str) -> std::vector<int> {
  auto vec = std::vector<int>();

  vec.reserve(32);

  auto iss = std::istringstream(str);

  while (iss) {
    auto value = 0;

    iss >> value;

    iss.ignore();

    vec.emplace_back(value);
  }

  return vec;
}

auto run() noexcept -> void {
  auto str = std::string();

  std::cin >> str;

  auto integers = parse_ints(str);

  for (auto it = std::begin(integers); it != std::end(integers); ++it) {
    std::cout << *it << '\n';
  }
}
} // namespace hr::cpp::string_stream

namespace hr::cpp::attribute_parser {
static auto skip_white_space(std::istream &is) noexcept -> void {
  for (; std::isspace(is.peek()) != 0; is.ignore()) {}
}

auto run() noexcept -> void {
  const auto input = R"(10 10
<a value = "GoodVal">
<b value = "BadVal" size = "10">
</b>
<c height = "auto">
<d size = "3">
<e strength = "2">
</e>
</d>
</c>
</a>
a~value
b~value
a.b~size
a.b~value
a.b.c~height
a.c~height
a.d.e~strength
a.c.d.e~strength
d~sze
a.c.d~size)";

  auto is = std::istringstream(input);

  auto number_of_tags{0ULL};

  auto number_of_queries{0ULL};

  is >> number_of_tags >> number_of_queries;

  skip_white_space(is);

  std::cout << number_of_tags << '\n';

  std::cout << number_of_queries << '\n';

  const auto output = R"(GoodVal
Not Found!
10
BadVal
Not Found!
auto
Not Found!
2
Not Found!
3)";

  std::cout << "\nExpected output: \n" << output << '\n';
}
} // namespace hr::cpp::attribute_parser

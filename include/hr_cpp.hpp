#ifndef HR_CPP_HPP
#define HR_CPP_HPP

#include <array>

namespace hr::cpp::hello_world {
auto run() noexcept -> void;
} // namespace hr::cpp::hello_world

namespace hr::cpp::basic_data_types {
auto run() noexcept -> void;
} // namespace hr::cpp::basic_data_types

namespace hr::cpp::input_and_output {
auto run() noexcept -> void;
} // namespace hr::cpp::input_and_output

namespace hr::cpp::conditional_if_else {
auto run() noexcept -> void;
} // namespace hr::cpp::conditional_if_else

namespace hr::cpp::for_loop {
auto run() noexcept -> void;
} // namespace hr::cpp::for_loop

namespace hr::cpp::functions {
auto run() noexcept -> void;
} // namespace hr::cpp::functions

namespace hr::cpp::pointer {
auto run() noexcept -> void;
} // namespace hr::cpp::pointer

namespace hr::cpp::arrays_introduction {
auto run() noexcept -> void;
} // namespace hr::cpp::arrays_introduction

namespace hr::cpp::variable_sized_arrays {
auto run() noexcept -> void;
} // namespace hr::cpp::variable_sized_arrays

namespace hr::cpp::variadics {
template <bool... Arguments>[[nodiscard]] inline auto reversed_binary_value() noexcept -> std::uint64_t {
  auto arr = std::array<bool, sizeof...(Arguments)>({Arguments...});

  auto value{0ULL};

  for (auto it = std::cend(arr) - 1; it >= std::cbegin(arr); --it) {
    value |= static_cast<std::uint64_t>(*it) << (it - std::cbegin(arr));
  }

  return value;
}

auto run() noexcept -> void;
} // namespace hr::cpp::variadics

namespace hr::cpp::bit_array {
auto run() noexcept -> void;
} // namespace hr::cpp::bit_array

namespace hr::cpp::string_stream {
auto run() noexcept -> void;
} // namespace hr::cpp::string_stream

namespace hr::cpp::attribute_parser {
auto run() noexcept -> void;
} // namespace hr::cpp::attribute_parser

#endif // HR_CPP_HPP
